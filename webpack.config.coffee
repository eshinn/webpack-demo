webpack = require 'webpack'
HtmlWebPackPlugin = require 'html-webpack-plugin'
validate = require 'webpack-validator'
CleanWebpackPlugin = require 'clean-webpack-plugin'
ExtractTextPlugin = require 'extract-text-webpack-plugin'
PurifyCSSPlugin = require 'purifycss-webpack-plugin'

TARGET = process.env.npm_lifecycle_event
PATHS =
  app: "#{__dirname}/app"
  style: [
    "#{__dirname}/node_modules/purecss"
    "#{__dirname}/app/main.css"
  ]
  build: "#{__dirname}/build"

console.log "FYI: TARGET is #{TARGET}"


BUILD =
  common:
    entry: switch TARGET
      when 'build'
        app: PATHS.app
        style: PATHS.style
        vendor: ['react']
      else
        app: PATHS.app
        style: PATHS.style
    output: switch TARGET
      when 'build'
        path: PATHS.build
        filename: '[name].[chunkhash].js'
        chunkFilename: '[chunkhash].js'
      else
        path: PATHS.build
        filename: '[name].js'
    module:
      loaders: [
      ].concat switch TARGET
        when 'dev' then [
            test: /\.css$/
            loaders: ['style', 'css']
            include: PATHS.style
        ]
        when 'build' then [
            test: /\.css$/
            loader: ExtractTextPlugin.extract 'style', 'css'
            include: PATHS.style
        ]
    plugins: [
      new HtmlWebPackPlugin
        title: 'Webpack demo'
    ].concat switch TARGET
      when 'dev' then [
        new webpack.HotModuleReplacementPlugin
          multiStep: true
      ]
      when 'build' then [
        new CleanWebpackPlugin [PATHS.build], {root: "#{__dirname}"}
        new webpack.DefinePlugin 'process.env.NODE_ENV', 'production'
        new webpack.optimize.CommonsChunkPlugin
          names: ['vendor', 'manifest']
          minChunks: Infinity
        new webpack.optimize.UglifyJsPlugin
          compress:
            warnings: false
          # mangle:
          #   props: /matching_props/
          #   except: [
          #     'Array'
          #     'BigInteger'
          #     'Boolean'
          #     'Buffer'
          #   ]
        new ExtractTextPlugin '[name].[chunkhash].css'
        new PurifyCSSPlugin
          basePath: "#{__dirname}"
          paths: PATHS.style
      ]
  dev:
    watchOptions:
      poll: true
    devtool: 'eval-source-map'
    devServer:
      historyApiFallback: true
      hot: true
      inline: true
      progress: true
      stats: 'errors-only'
      host: process.env.HOST
      port: process.env.PORT

do ({common: build, "#{TARGET}": target} = BUILD) ->
  build[key] = val for key, val of target
  module.exports = validate build
